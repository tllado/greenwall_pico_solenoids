# greenwall_pico_solenoids

Control code for my greenwall using Raspberry Pi Pico.

This project controls an array of solenoid valves. It replaces a previous project that controlled a motor-operated distributor valve. The solenoid array is physically larger but cheaper and more reliable.
