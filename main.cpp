////////////////////////////////////////////////////////////////////////////////
// Greenwall Control Code
// written by Travis Llado
// last edited 2023-01-22
////////////////////////////////////////////////////////////////////////////////

#include <vector>
#include "hardware/gpio.h"
#include "hardware/rtc.h"
#include "pico/stdlib.h"
#include "pico/util/datetime.h"

////////////////////////////////////////////////////////////////////////////////

#define SECOND  (1U)
#define MINUTE  (60U*SECOND)
#define HOUR    (60U*MINUTE)

#define START_TIME  (8U*HOUR + 0U*MINUTE + 0U*SECOND)  // time of day

enum LogicType {direct, inverted};

class Load {
public:
  Load(uint32_t pin_number, LogicType logic) : pin_number(pin_number), logic(logic) {
    init();
  }
  void init() {
    gpio_init(pin_number);
    gpio_set_dir(pin_number, GPIO_OUT);

    switch(logic) {
      case LogicType::direct:
        gpio_pull_down(pin_number);
        break;
      case LogicType::inverted:
        gpio_pull_up(pin_number);
        break;
      default:
        // do nothing
        break;
    }

    off();
  }
  void on() {
    gpio_put(pin_number, (logic == LogicType::direct));
  }
  void off() {
    gpio_put(pin_number, (logic == LogicType::inverted));
  }
  const uint32_t pin_number;
  const LogicType logic;
};

class Solenoid : public Load {
public:
  Solenoid(uint32_t pin_number, uint32_t run_time) : Load(pin_number, LogicType::inverted), run_time(run_time) {}
  const uint32_t run_time;
};

#define TIME_LIGHTS_ON  (8U*HOUR)   // 8:00AM
#define TIME_LIGHTS_OFF (20U*HOUR)  // 8:00PM
#define DURATION_SWITCH 200U        // milliseconds
#define DURATION_PUMP   12000U      // milliseconds
#define DURATION_STEP   1000U       // milliseconds

const std::vector<uint32_t> watering_times {
  (9U*HOUR), // 9:00 AM
  (19U*HOUR) // 7:00 PM
};

Load lights(28U, LogicType::direct);
Load pump(27U, LogicType::inverted);
std::vector<Solenoid> solenoids {
  Solenoid(26U, (DURATION_PUMP + DURATION_STEP * 12U)), // Row 01
  Solenoid(22U, (DURATION_PUMP + DURATION_STEP * 11U)), // Row 02
  Solenoid(21U, (DURATION_PUMP + DURATION_STEP * 10U)), // Row 03
  Solenoid(20U, (DURATION_PUMP + DURATION_STEP * 9U)),  // Row 04
  Solenoid(19U, (DURATION_PUMP + DURATION_STEP * 8U)),  // Row 05
  Solenoid(18U, (DURATION_PUMP + DURATION_STEP * 7U)),  // Row 06
  Solenoid(17U, (DURATION_PUMP + DURATION_STEP * 6U)),  // Row 07
  Solenoid(16U, (DURATION_PUMP + DURATION_STEP * 5U)),  // Row 08
  Solenoid(11U, (DURATION_PUMP + DURATION_STEP * 4U)),  // Row 09
  Solenoid(12U, (DURATION_PUMP + DURATION_STEP * 3U)),  // Row 10
  Solenoid(13U, (DURATION_PUMP + DURATION_STEP * 2U)),  // Row 11
  Solenoid(14U, (DURATION_PUMP + DURATION_STEP * 1U)),  // Row 12
  Solenoid(15U, (DURATION_PUMP + DURATION_STEP * 0U)),  // Row 13
};

////////////////////////////////////////////////////////////////////////////////

void program_update(uint32_t now) {
  // Update Grow Lights
  if ((now >= TIME_LIGHTS_ON) && (now < TIME_LIGHTS_OFF)) {
    lights.on();
  } else {
    lights.off();
  }

  // Update Watering System
  bool time_to_start_watering{false};
  for (auto watering_time : watering_times) {
    time_to_start_watering |= (now == watering_time);
  }

  if (time_to_start_watering) {
    for (uint32_t i = 0U; i < solenoids.size(); i++) {
      if (i == 0U) {
        solenoids[0U].on();
        sleep_ms(DURATION_SWITCH);
        pump.on();
        gpio_put(PICO_DEFAULT_LED_PIN, true);
      } else {
        solenoids[i - 1U].off();
      }

      sleep_ms(solenoids[i].run_time);

      if (i == (solenoids.size() - 1U)) {
        pump.off();
        gpio_put(PICO_DEFAULT_LED_PIN, false);
        sleep_ms(DURATION_SWITCH);
        solenoids[i].off();
      } else {
        solenoids[i + 1U].on();
        gpio_put(PICO_DEFAULT_LED_PIN, (i % 2));
        sleep_ms(DURATION_SWITCH);
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

uint32_t time_of_day_sec(datetime_t& now) {
  return now.hour*HOUR + now.min*MINUTE + now.sec*SECOND;
}

////////////////////////////////////////////////////////////////////////////////

int main() {
  // datetime_t must be initialized with year, month, and day or it will not run
  datetime_t now = {
    .year  = 0U,
    .month = 1U,
    .day   = 1U,
    .dotw  = 0U,
    .hour  = (START_TIME/HOUR),
    .min   = ((START_TIME%HOUR)/MINUTE),
    .sec   = (START_TIME%MINUTE)
  };

  rtc_init();
  rtc_set_datetime(&now);
  uint32_t prev_sec = now.sec;

  gpio_init(PICO_DEFAULT_LED_PIN);
  gpio_set_dir(PICO_DEFAULT_LED_PIN, GPIO_OUT);
  gpio_put(PICO_DEFAULT_LED_PIN, false);

  while (true) {
    rtc_get_datetime(&now);

    if (now.sec != prev_sec) {
      prev_sec = now.sec;
      program_update(time_of_day_sec(now));
    } else {
      sleep_ms(500U);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////
// End of file
////////////////////////////////////////////////////////////////////////////////
